import random
import time
import os
import sys
import pkg_resources


def cls():
    os.system('cls' if os.name == 'nt' else 'clear')


def colored(r, g, b, text):
    return "\033[38;2;{};{};{}m{} \033[38;2;255;255;255m".format(r, g, b, text)


def fight(player, villain):
    player_dmg_done = 0
    villain_dmg_done = 0
    print()
    while player.get("hp") >= villain_dmg_done or villain.get("hp") >= player_dmg_done:
        x = random.randint(1, 10)
        if x < 8:
            player_shoot = random.randint(player.get("min_dmg"), player.get("max_dmg"))
            print(player.get("battle_name") + ' shooted: ' + str(player_shoot) + ' dmg to ' + villain.get("battle_name"))
        else:
            player_shoot = random.randint(player.get("min_dmg"), player.get("max_dmg")) * 2
            print(player.get("battle_name") + ' shooted: ' + str(player_shoot) +  f' {colored(255,0,0,"CRITICAL")} dmg to ' + villain.get("battle_name"))
        player_dmg_done += player_shoot
        left = -(player_dmg_done - villain.get("hp"))
        print('HP left: ' + str(left) + ' from ' + str(villain.get("hp")))
        print()
        if player_dmg_done > villain.get("hp"):
            status = colored(0,250,0,f"YOU WON! {player.get('battle_name')} survived.")
            print(status)
            print('Meanwhile ' + villain.get("battle_name") + ' is destroyed.')
            return 0
        time.sleep(0.5)
        y = random.randint(1, 10)
        if y < 8:
            villain_shoot = random.randint(villain.get("min_dmg"), villain.get("max_dmg"))
            print(villain.get("battle_name") + ' shooted: ' + str(villain_shoot) + ' dmg to ' + player.get("battle_name"))
        else:
            villain_shoot = random.randint(villain.get("min_dmg"), villain.get("max_dmg")) * 2
            print(villain.get("battle_name") + ' shooted: ' + str(villain_shoot) + f' {colored(255,0,0,"CRITICAL")} dmg to ' + player.get("battle_name"))
        villain_dmg_done += villain_shoot
        left = -(villain_dmg_done - player.get("hp"))
        print('HP left: ' + str(left) + ' from ' + str(player.get("hp")))
        print()
        if villain_dmg_done > player.get("hp"):
            status = colored(250,0,0, f"YOU LOST... {villain.get('battle_name')} survived.")
            print(status)
            print('Meanwhile ' + player.get("battle_name") + ' is destroyed.')
            return 0
        time.sleep(0.5)


def welcome_screen(version):
    print('\n\n           ============= Welcome to TANK MASTERS =============')
    time.sleep(0.4)
    print('\n\n              ============= created by commRat =============')
    time.sleep(0.4)
    print('\n\n                    ============= 2020 =============')
    time.sleep(0.4)
    print(f'\n\n                                = v{version} =')
    time.sleep(3)
    cls()
    return 0


def exit_screen():
    cls()
    input('Press ENTER for exit.')
    print('\n\n       =============Thanks for playing!=============')
    time.sleep(1)
    print('\n\n       ============= See you next time =============')
    time.sleep(3)
    cls()
    return 0


def main():
    version = pkg_resources.resource_string(__name__, "VERSION").decode().strip()
    welcome_screen(version)
    tank_list = pkg_resources.resource_string(__name__, "data/tanks.txt")
    while True:
        print('Pick your tank. (Press the number and hit enter). For exit the application, press ENTER.')
        print(tank_list.decode())
        player_tank = input('')
        cls()
        print('Pick enemy tank. (Press the number and hit enter). For exit the application, press ENTER.')
        print(tank_list.decode())
        villain_tank = input('')
        cls()
        specifications = {
            '1': {"hp": 3000, "min_dmg": 100, "max_dmg": 200, "battle_name": 'Abrams'},
            '2': {"hp": 2000, "min_dmg": 150, "max_dmg": 350, "battle_name": 'Armata'},
            '3': {"hp": 1500, "min_dmg": 200, "max_dmg": 450, "battle_name": 'Sprut'},
            '4': {"hp": 3500, "min_dmg": 50, "max_dmg": 150, "battle_name": 'K2 Black Panther'},
            '5': {"hp": 2500, "min_dmg": 120, "max_dmg": 300, "battle_name": 'Type 15'},
            '6': {"hp": random.randint(1000, 3500), "min_dmg": random.randint(1, 200), "max_dmg": random.randint(220, 450), "battle_name": 'Mystery tank'},
        }
        available_tanks = ["1", "2", "3", "4", "5", "6"]
        if villain_tank in available_tanks and player_tank in available_tanks:
            player = specifications[player_tank]
            villain = specifications[villain_tank]
            fight(player, villain)
        else:
            print('Wrong input. You have to provide number in range: 1-6')

        again = input('Do yo wish to continue? Write Y and press enter. \nIf you want quit, just press enter.')
        if again.lower() == 'y':
            cls()
        else:
            exit_screen()
            return 0


if __name__ == '__main__':
    sys.exit(main())

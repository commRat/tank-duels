FROM python:3.10-alpine
RUN apk --update add gcc build-base freetype-dev libpng-dev openblas-dev
RUN mkdir home/tankduels 
WORKDIR home/tankduels
COPY . .
ENTRYPOINT ["python", "tankduels/tankduels.py"] 

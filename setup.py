#!/usr/bin/python

from setuptools import setup, find_packages
import pathlib
from setuptools.command.install_scripts import install_scripts
import subprocess
from distutils.util import convert_path
import os
import subprocess

here = pathlib.Path(__file__).parent.resolve()
long_description = (here / 'README.md').read_text(encoding='utf-8')
path_ = subprocess.run("pwd", capture_output=True, shell=True).stdout.decode().strip() + "/"
with open(os.path.join(path_, 'tankduels/VERSION')) as version_file:
    __version__ = version_file.read().strip()

setup(
    name='tankduels',
    version=__version__,
    description='Console game, where player choose a tank and fight enemy. Damage is randomly generated.',
    long_description=long_description,
    url='https://gitlab.com/commRat/tank-duels',
    author='David Tomicek',
    author_email='tomicek.david@yahoo.com',
    keywords='console game, random, tanks, cli',
    packages=find_packages(),
    package_data={'tankduels': [
        "data/tanks.txt",
        'VERSION',
        ]},
    entry_points={"console_scripts": ["tankduels = tankduels.tankduels:main"]},
)
